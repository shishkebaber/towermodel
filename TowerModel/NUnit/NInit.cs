﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TowerModel;

namespace Tests
    {
    [TestFixture]
    public class Test
    {
        [TestCase(100, 10, 500, 500, 50, 100, 25, 50, 500, true, TestName = "Тестируем минимальные допустимые значения")]
        [TestCase(650, 499, 1300, 1300, 650, 2000, 325, 1000, 1300, true, TestName = "Тестируем максимальные допустимые значения")]
        [TestCase(500, 100, 1000, 1000, 200, 100, 100, 100, 1000, true, TestName = "Тестируем допустимые значения")]
        [TestCase(651, 500, 1301, 1301, 651, 2001, 326, 1001, 1301, true, TestName = "Тестируем значение выше максимального допустимого", ExpectedException = typeof(Exception))]
        [TestCase(99, 9, 499, 499, 49, 99, 9, 9, 499, true, TestName = "Тестируем значение ниже минимально допустимого", ExpectedException = typeof(Exception))]
        [TestCase(501, 100, 1000, 1000, 200, 100, 100, 100, 1000, true, TestName = "Тестируем превышение радиуса арки относительно половины высоты основания", ExpectedException = typeof(Exception))]
        [TestCase(500, 100, 1000, 1000, 300, 100, 100, 100, 1000, true, TestName = "Тестируем превышение высоты короны относительно удвоенной высоты перегородки", ExpectedException = typeof(Exception))]
        [TestCase(500, 100, 1000, 1000, 200, 300, 100, 100, 1000, true, TestName = "Тестируем превышение толщины короны относительно удвоенной ширины выступа", ExpectedException = typeof(Exception))]
        
        public void ParametresTest(int archRadius, int balkThickness, int bottomPartLength, int centerPartHeight,
                        int crownHeight, int crownThickness, int protrusionHeight, int protrusionWidth, int topPartHeight, Boolean isCrown)
        {
              Parametres parametres = new Parametres(archRadius, balkThickness, bottomPartLength, centerPartHeight, crownHeight, 
                                              crownThickness, protrusionHeight, protrusionWidth, topPartHeight, isCrown);
              parametres.Validate();
        }
}
}

