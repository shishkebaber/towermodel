﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerModel
{
    /// <summary>
    /// Класс в котором хранятся параметры башни
    /// </summary>
    public class Parametres
    {
        #region Properties

        /// <summary>
        /// Радиус основания
        /// </summary>
        public int ArchRadius {get; private set; }

        /// <summary>
        /// Толщина балки
        /// </summary>
        public int BalkThickness { get; private set; }

        /// <summary>
        /// Длина основания
        /// </summary>
        public int BottomPartLength { get; private set; }

        /// <summary>
        /// Высота цетральной части
        /// </summary>
        public int CenterPartHeight { get; private set; }

        /// <summary>
        /// Высота короны
        /// </summary>
        public int CrownHeight { get; private set; }

        /// <summary>
        /// Толщина короны
        /// </summary>
        public int CrownThickness { get; private set; }

        /// <summary>
        /// Высота выступа
        /// </summary>
        public int ProtrusionHeight { get; private set; }


        /// <summary>
        /// Ширина выступа
        /// </summary>
        public int ProtrusionWidth { get; private set; }

        /// <summary>
        /// Высота верхней части
        /// </summary>
        public int TopPartHeight { get; private set; }


        /// <summary>
        /// Наличие короны
        /// </summary>
        public Boolean IsCrown { get; private set; }

    

        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="archRadius"></param>
        /// <param name="balkThickness"></param>
        /// <param name="bottomPartLength"></param>
        /// <param name="centerPartHeight"></param>
        /// <param name="crownHeight"></param>
        /// <param name="crownThickness"></param>
        /// <param name="protrusionHeight"></param>
        /// <param name="protrusionWidth"></param>
        /// <param name="topPartHeight"></param>
        /// <param name="isCrown"></param>
        public Parametres(int archRadius, int balkThickness, int bottomPartLength, int centerPartHeight, 
            int crownHeight, int crownThickness, int protrusionHeight, int protrusionWidth, int topPartHeight, Boolean isCrown)
        {
            this.ArchRadius = archRadius;
            this.BalkThickness = balkThickness;
            this.BottomPartLength = bottomPartLength;
            this.CrownHeight = crownHeight;
            this.CenterPartHeight = centerPartHeight;
            this.CrownThickness = crownThickness;
            this.ProtrusionHeight = protrusionHeight;
            this.ProtrusionWidth = protrusionWidth;
            this.TopPartHeight = topPartHeight;
            this.IsCrown = isCrown;
        }

        /// <summary>
        /// Валидация значений
        /// </summary>
        public void Validate()
        {
            string _errors = string.Empty;

            if (this.ArchRadius < 100 || this.ArchRadius > this.BottomPartLength / 2)
            {
                _errors += "Радиус арки должен быть больше 100 и меньше половины высоты основания \n";
            }
            //TODO:
            if (HeightValidate(this.BottomPartLength))
            {
                _errors += "Высота нижней части должна быть больше 500 и меньше 1300 \n";
            }
            if (HeightValidate(this.CenterPartHeight))
            {
                _errors += "Высота центральной части должна быть больше 500 и меньше 1300 \n";
            }
            if (HeightValidate(this.TopPartHeight))
            {
                _errors += "Высота верхней части должна быть больше 500 и меньше 1300 \n";
            }
            if (this.BalkThickness < 10 || this.BalkThickness > 499 )
            {
                _errors += "Толщина балки должна быть больше 10 и меньше 499 \n";
            }
            if (this.CrownHeight < 50 || this.CrownHeight > this.ProtrusionHeight * 2)
            {
                _errors += "Высота короны должна быть больше 50 и меньше удвоенной высоты выступа\n";
            }
            if (this.CrownThickness < 100 || this.CrownThickness > this.ProtrusionWidth * 2)
            {
                _errors += "Толщина короны должна быть больше 100 и меньше удвоенной ширины выступа\n";
            }
            if (this.ProtrusionHeight < 25 || this.ProtrusionHeight > 325)
            {
                _errors += "Высота выступа должна быть больше 25 и меньше 325\n";
            }
            if(this.ProtrusionWidth < 50 || this.ProtrusionWidth > 1000)
            {
                _errors += "Ширина выступа должна быть больше 50 и меньше 1000 \n";
            }
            

            if (_errors.Length != 0)
            {
                throw new ApplicationException(_errors);
            }

        }

        /// <summary>
        /// Валидация высоты
        /// </summary>
        /// <param name="height"> Высота </param>
        /// <returns> True если высота не входит в разрешенные дипазон </returns>
        private Boolean HeightValidate(int height)
        {
            return height < 500 || height > 1300;
        }

    }
}
