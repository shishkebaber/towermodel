﻿
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TowerModel.Objects;

namespace TowerModel
{
    /// <summary>
    /// Класс выполняющий работу с документом и базой данных AutoCAD
    /// </summary>
    class BuildManager
    {
        /// <summary>
        /// База документа
        /// </summary>
        private Database _database;

        /// <summary>
        /// Документ Autocad
        /// </summary>
        private Document _document;

        /// <summary>
        ///  
        /// </summary>
        private Editor _editor;

        /// <summary>
        /// Конструктор
        /// </summary>
        protected BuildManager()
        {
            _document = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;
            _database = _document.Database;
        }

        /// <summary>
        /// Фабрика для BuildManager
        /// </summary>
        private sealed class ManagerCreator
        {
            private static readonly BuildManager _instance = new BuildManager();
            public static BuildManager Instance { get { return _instance; } }
        }

        
        /// <summary>
        /// Возвращаемая сущность BuildManager
        /// </summary>
        public static BuildManager Instance
        {
            get { return ManagerCreator.Instance; }
        }


        /// <summary>
        /// Метод в котором строится сущность
        /// </summary>
        /// <param name="entity"> Сущность для построения </param>
        public void BuildEntity(Entity entity)
        {
             _editor = _document.Editor;

            // Начало транзакции
            using (var documentLock = _document.LockDocument())
            {
                using (var transaction = _database.TransactionManager.StartTransaction())
                {

                    // Открытие таблицы документа
                    var blockTable = transaction.GetObject(_database.BlockTableId, OpenMode.ForRead) as BlockTable;
                    var blockTableRecord =
                                transaction.GetObject(blockTable[BlockTableRecord.ModelSpace], OpenMode.ForWrite)
                                as BlockTableRecord;
                    // Переключение стиля
                    ViewportTable viewportTable =
                        (ViewportTable)transaction.GetObject(_database.ViewportTableId, OpenMode.ForRead);
                    ViewportTableRecord viewportTableRecord =
                        (ViewportTableRecord)transaction.GetObject(viewportTable["*Active"], OpenMode.ForWrite);
                    DBDictionary dictionary =
                        (DBDictionary)transaction.GetObject(_database.VisualStyleDictionaryId, OpenMode.ForRead);
                    viewportTableRecord.VisualStyleId = dictionary.GetAt("Realistic");

                    // Добавление сущности в таблицу
                    blockTableRecord.AppendEntity(entity);
                    transaction.AddNewlyCreatedDBObject(entity, true);

                    //Сохранение
                    transaction.Commit();
                    _editor.UpdateTiledViewportsFromDatabase();
                   
                }
            }
        }

        /// <summary>
        /// Метод для очистки экрана
        /// </summary>
        public void Clear()
        {
            Document document = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;
            Database dataBase = document.Database;
            using (var documentLock = document.LockDocument())
            {
                using (Transaction transaction = dataBase.TransactionManager.StartTransaction())
                {
                    var prompt = document.Editor.SelectAll(); 
                    var set = prompt.Value;
                    if (set != null)
                    {
                        foreach (SelectedObject obj in set)
                        {
                            if (obj != null)
                            {
                                Entity entity = transaction.GetObject(obj.ObjectId, OpenMode.ForWrite) as Entity;
                                if (entity != null)
                                {
                                    entity.Erase(true);
                                }
                            }
                        }
                    }
                    transaction.Commit();
                }
            }
        }

    }
}
