﻿using Autodesk.AutoCAD.Runtime;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TowerModel.Objects;

namespace TowerModel
{
    /// <summary>
    /// Класс хранящий в себе команды для работы с AutoCAD
    /// </summary>
    public class Commands : IExtensionApplication
    {
        /// <summary>
        /// Функция инициализации (выполняется при загрузке плагина)
        /// </summary> 
        public void Initialize()
        {
            Application.Run(new FormTower());
        }

        /// <summary>
        /// Функция выполняемая при выгрузке плагина
        /// </summary>
        public void Terminate() { }

        /// <summary>
        /// Эта функция будет вызываться при выполнении в AutoCAD команды «ShowForm»
        /// </summary>
        [CommandMethod("ShowForm")]
        public void MyCommand()
        {
            Application.Run(new FormTower());
        }

        /// <summary>
        /// Нагрузочное тестирование
        /// </summary>
        [CommandMethod("TESTING")]
        public void Test()
        {
            System.Diagnostics.Stopwatch myStopwatch = new System.Diagnostics.Stopwatch();
            StreamWriter file = new StreamWriter("D:\\TestORSAPR.txt");
            for (int i = 0; i < 100; i++)
            {
                myStopwatch.Reset();
                myStopwatch.Start(); 

                // Строим со значениями по умолчанию 
                Parametres parametersPhoto = new Parametres(500, 100, 1000, 1000, 200, 100, 100, 100, 1000, true);
                Tower tower = new Tower(parametersPhoto);
                tower.BuildTower();

                myStopwatch.Stop(); 

                TimeSpan ts = myStopwatch.Elapsed;
                string elapsedTime = String.Format("{0:f}", ts.Milliseconds);
                file.Write(elapsedTime + "\n");
            }
            file.Close();
        }
    }
}
