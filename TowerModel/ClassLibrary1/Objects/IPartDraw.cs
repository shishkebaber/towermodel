﻿using Autodesk.AutoCAD.DatabaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerModel.Objects
{
    /// <summary>
    /// Интерфейс описывающий поведение частей башни
    /// </summary>
    public interface IPartDraw
    {
         Entity Entity { get; }
    }
}
