﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerModel.Objects
{
    /// <summary>
    /// Класс в котором происходит построение всех частей башни
    /// </summary>
    class Tower
    {
        /// <summary>
        /// Список частей которые строит класс
        /// </summary>
        private List<IPartDraw> _parts;

        /// <summary>
        /// Ширина основания
        /// </summary>
        const int BOTTOM_WIDTH = 2000;

        /// <summary>
        ///  Конструктор
        /// </summary>
        /// <param name="parametres"></param>
        public Tower(Parametres parametres)
        {
            _parts = new List<IPartDraw>();
            _parts.Add(new BottomPart(parametres, BOTTOM_WIDTH));
            _parts.Add(new MiddlePart(parametres, BOTTOM_WIDTH));
            _parts.Add(new TopPart(parametres, BOTTOM_WIDTH));
        }

        /// <summary>
        /// Метод выполняющий построение частей башни
        /// </summary>
        public void BuildTower()
        {
            // Очистка экрана
            BuildManager.Instance.Clear();

            // Построение частей
            foreach (IPartDraw part in _parts)
            {
                BuildManager.Instance.BuildEntity(part.Entity);
            }
        }

    }
}
