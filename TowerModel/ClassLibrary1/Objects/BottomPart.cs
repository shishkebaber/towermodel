﻿using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerModel.Objects
{
    /// <summary>
    /// Класс, который строит нижнюю часть башни
    /// </summary>
    class BottomPart : IPartDraw
    {
        /// <summary>
        /// Класс параметров
        /// </summary>
        private Parametres _parametres;

        /// <summary>
        /// Возвращаемая сущность
        /// </summary>
        public Entity Entity 
        {
            get
            {
                return BuildEntity();
            }
        }

        /// <summary>
        /// Ширина нижней части
        /// </summary>
        private int _width;

        /// <summary>
        /// Количество сторон пирамиды
        /// </summary>
        const int SIDES = 4;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="parametres"> Параметры </param>
        /// <param name="bottomWidth"> Ширина нижней части </param>
        public BottomPart(Parametres parametres, int bottomWidth)
        {
            _parametres = parametres;
            _width = bottomWidth;
        }

        /// <summary>
        /// Метод построения основания
        /// </summary>
        /// <returns> Сущность нижней части </returns>
        private Entity BuildEntity()
        {
            Solid3d bottomPart = new Solid3d();
            bottomPart.CreatePyramid(_parametres.BottomPartLength, SIDES, _width, _width / 2);
            bottomPart.Color = Color.FromNames(Properties.Resources.Gray, Properties.Resources.Metallic);
            bottomPart.SetDatabaseDefaults();

            //Оси вращения
            CoordinateSystem3d coordinateSystem = Autodesk.AutoCAD.ApplicationServices.Application.
                          DocumentManager.MdiActiveDocument.Editor.CurrentUserCoordinateSystem.CoordinateSystem3d;
            Vector3d xAxis = coordinateSystem.Xaxis;
            Vector3d yAxis = coordinateSystem.Yaxis;
            Vector3d zAxis = coordinateSystem.Zaxis;


            //Арки
            Solid3d holeX = new Solid3d();
            Solid3d holeY = new Solid3d();
            Solid3d protrusion = new Solid3d();
            holeX.CreateFrustum(_width * 2, _parametres.ArchRadius * 2, _parametres.ArchRadius * 2, _parametres.ArchRadius * 2);
            holeY.CreateFrustum(_width * 2, _parametres.ArchRadius * 2, _parametres.ArchRadius * 2, _parametres.ArchRadius * 2);
            protrusion.CreateBox(_width  + _parametres.ProtrusionWidth, 
                                 _width + _parametres.ProtrusionWidth, _parametres.ProtrusionHeight);
            protrusion.Color = Color.FromNames(Properties.Resources.Gray, Properties.Resources.Metallic);

            //Вращение
            holeX.TransformBy(Matrix3d.Rotation(Math.PI / 2, xAxis, new Point3d(0, 0, 0)));
            holeY.TransformBy(Matrix3d.Rotation(Math.PI / 2, yAxis, new Point3d(0, 0, 0)));

            //Смещение
            holeX.TransformBy(Matrix3d.Displacement((new Point3d(0, 0,
                             -_parametres.BottomPartLength / 2)).GetAsVector()));
            holeY.TransformBy(Matrix3d.Displacement((new Point3d(0, 0,
                            -_parametres.BottomPartLength / 2)).GetAsVector()));
            protrusion.TransformBy(Matrix3d.Displacement((new Point3d(0, 0,
                            _parametres.BottomPartLength / 2)).GetAsVector()));

            //Вырезание 
            bottomPart.BooleanOperation(BooleanOperationType.BoolSubtract, holeX);
            bottomPart.BooleanOperation(BooleanOperationType.BoolSubtract, holeY);
            bottomPart.BooleanOperation(BooleanOperationType.BoolUnite, protrusion);

            return bottomPart;
        }
    }
}
