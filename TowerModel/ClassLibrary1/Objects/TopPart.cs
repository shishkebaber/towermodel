﻿using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerModel.Objects
{
    /// <summary>
    /// Класс, который строит верхнюю часть башни
    /// </summary>
    class TopPart : IPartDraw
    {
        /// <summary>
        /// Парамаетры 
        /// </summary>
        private Parametres _parametres;

        /// <summary>
        /// Ширина верхней части
        /// </summary>
        private int _width;
        

        /// <summary>
        /// Количество сторон пирамиды
        /// </summary>
        const int SIDES = 4;

        /// <summary>
        /// Возвращаемая сущность
        /// </summary>
        public Entity Entity 
        {
            get
            {
                return BuildEntity();
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="parametres"> Параметры </param>
        /// <param name="bottomWidth"> Ширина нижней части </param>
        public TopPart(Parametres parametres, int bottomWidth)
        {
            _parametres = parametres;
            _width = bottomWidth / 4;
        }

        /// <summary>
        /// Метод построения верхней части
        /// </summary>
        /// <returns> Сущность верхней части </returns>
        private Entity BuildEntity()
        {

            // Процентные части от ширины
            int boxWidth = _width * 2 / 10;
            int innerWidth = _width * 4 / 10;

            //Верхняя часть
            Solid3d topPart = new Solid3d();
            topPart.CreatePyramid(_parametres.TopPartHeight, SIDES, _width, _width / 2);
            topPart.Color = Color.FromNames(Properties.Resources.Gray, Properties.Resources.Metallic);
            
            topPart.SetDatabaseDefaults();

            Solid3d innerHole = new Solid3d();
            innerHole.CreatePyramid(_parametres.TopPartHeight, SIDES, innerWidth,
                innerWidth);

            //Оси вращения
            CoordinateSystem3d coordinateSystem = Autodesk.AutoCAD.ApplicationServices.Application.
                        DocumentManager.MdiActiveDocument.Editor.CurrentUserCoordinateSystem.CoordinateSystem3d;
            Vector3d xAxis = coordinateSystem.Xaxis;
            Vector3d yAxis = coordinateSystem.Yaxis;
            Vector3d zAxis = coordinateSystem.Zaxis;
            
            // Смещение
            topPart.TransformBy(Matrix3d.Displacement((new Point3d(0, 0,
                             _parametres.BottomPartLength / 2 + _parametres.CenterPartHeight + _parametres.ProtrusionHeight * 1.5
                             + _parametres.TopPartHeight / 2)).GetAsVector()));
            innerHole.TransformBy(Matrix3d.Displacement((new Point3d(0, 0,
                             _parametres.BottomPartLength / 2 + _parametres.CenterPartHeight + _parametres.ProtrusionHeight * 1.5
                             + _parametres.TopPartHeight / 2)).GetAsVector()));
            
            //Вырезание полости
            topPart.BooleanOperation(BooleanOperationType.BoolSubtract, innerHole);

            //Вырезание решетки
            Solid3d yBottomBox1 = new Solid3d();
            yBottomBox1.CreateBox(boxWidth,
                _width * 2, _parametres.TopPartHeight / 3 );

            Solid3d yBottomBox2 = new Solid3d();
            yBottomBox2.CreateBox(boxWidth,
                _width * 2, _parametres.TopPartHeight / 3 );

            Solid3d yTopBox1 = new Solid3d();
            yTopBox1.CreateBox(boxWidth,
                _width * 2, _parametres.TopPartHeight / 3 );

            Solid3d yTopBox2 = new Solid3d();
            yTopBox2.CreateBox(boxWidth,
                _width * 2, _parametres.TopPartHeight / 3 );

            Solid3d xBottomBox1 = new Solid3d();
            xBottomBox1.CreateBox(_width * 2,
               boxWidth, _parametres.TopPartHeight / 3);

            Solid3d xBottomBox2 = new Solid3d();
            xBottomBox2.CreateBox(_width * 2,
                boxWidth, _parametres.TopPartHeight / 3);

            Solid3d xTopBox1 = new Solid3d();
            xTopBox1.CreateBox(_width * 2,
                boxWidth, _parametres.TopPartHeight / 3);

            Solid3d xTopBox2 = new Solid3d();
            xTopBox2.CreateBox(_width * 2,
                boxWidth, _parametres.TopPartHeight / 3);


            // Смещение 
            yBottomBox1.TransformBy(Matrix3d.Displacement((new Point3d( boxWidth , 0,
                            _parametres.BottomPartLength / 2 + _parametres.CenterPartHeight + _parametres.ProtrusionHeight * 1.5
                            + _parametres.TopPartHeight / 4)).GetAsVector()));
            xBottomBox1.TransformBy(Matrix3d.Displacement((new Point3d(0, boxWidth ,
                            _parametres.BottomPartLength / 2 + _parametres.CenterPartHeight + _parametres.ProtrusionHeight * 1.5
                            + _parametres.TopPartHeight / 4)).GetAsVector()));

            yBottomBox2.TransformBy(Matrix3d.Displacement((new Point3d(boxWidth , 0,
                             _parametres.BottomPartLength / 2 + _parametres.CenterPartHeight + _parametres.ProtrusionHeight * 1.5
                             + _parametres.TopPartHeight / 2 + _parametres.TopPartHeight / 6)).GetAsVector()));
            xBottomBox2.TransformBy(Matrix3d.Displacement((new Point3d(0,  boxWidth ,
                             _parametres.BottomPartLength / 2 + _parametres.CenterPartHeight + _parametres.ProtrusionHeight * 1.5
                             + _parametres.TopPartHeight / 2 + _parametres.TopPartHeight / 6)).GetAsVector()));

            yTopBox1.TransformBy(Matrix3d.Displacement((new Point3d(- boxWidth , 0,
                             _parametres.BottomPartLength / 2 + _parametres.CenterPartHeight + _parametres.ProtrusionHeight * 1.5
                            + _parametres.TopPartHeight / 4)).GetAsVector()));
            xTopBox1.TransformBy(Matrix3d.Displacement((new Point3d(0, - boxWidth ,
                             _parametres.BottomPartLength / 2 + _parametres.CenterPartHeight + _parametres.ProtrusionHeight * 1.5
                            + _parametres.TopPartHeight / 4)).GetAsVector()));

            yTopBox2.TransformBy(Matrix3d.Displacement((new Point3d(- boxWidth, 0,
                             _parametres.BottomPartLength / 2 + _parametres.CenterPartHeight + _parametres.ProtrusionHeight * 1.5
                             + _parametres.TopPartHeight / 2 + _parametres.TopPartHeight / 6)).GetAsVector()));
            xTopBox2.TransformBy(Matrix3d.Displacement((new Point3d(0, - boxWidth,
                             _parametres.BottomPartLength / 2 + _parametres.CenterPartHeight + _parametres.ProtrusionHeight * 1.5
                             + _parametres.TopPartHeight / 2 + _parametres.TopPartHeight / 6)).GetAsVector()));

            // Вырезание 
            topPart.BooleanOperation(BooleanOperationType.BoolSubtract, yBottomBox1);
            topPart.BooleanOperation(BooleanOperationType.BoolSubtract, xBottomBox1);
            topPart.BooleanOperation(BooleanOperationType.BoolSubtract, yBottomBox2);
            topPart.BooleanOperation(BooleanOperationType.BoolSubtract, xBottomBox2);
            topPart.BooleanOperation(BooleanOperationType.BoolSubtract, yTopBox1);
            topPart.BooleanOperation(BooleanOperationType.BoolSubtract, xTopBox1);
            topPart.BooleanOperation(BooleanOperationType.BoolSubtract, yTopBox2);
            topPart.BooleanOperation(BooleanOperationType.BoolSubtract, xTopBox2);

            // Построение короны
            if (_parametres.IsCrown)
            {
                Solid3d crown = new Solid3d();
                crown.CreateBox(_width / 2 + _parametres.CrownThickness * 2, _width / 2 +
                    _parametres.CrownThickness * 2, _parametres.CrownHeight);
                crown.Color = Color.FromNames(Properties.Resources.Gray, Properties.Resources.Metallic);

                // Смещение короны 
                crown.TransformBy(Matrix3d.Displacement((new Point3d(0, 0,
                             _parametres.BottomPartLength / 2 + _parametres.CenterPartHeight +
                             _parametres.TopPartHeight + _parametres.ProtrusionHeight * 1.5 + _parametres.CrownHeight / 2)).GetAsVector()));
                
                // Отверстие в короне
                Solid3d hole = new Solid3d();
                hole.CreateBox(_width / 2, _width / 2, _parametres.CrownHeight);
                hole.Color = Color.FromNames(Properties.Resources.Gray, Properties.Resources.Metallic);
                // Смещение отверстия
                hole.TransformBy(Matrix3d.Displacement((new Point3d(0, 0,
                              _parametres.BottomPartLength / 2 + _parametres.CenterPartHeight +
                             _parametres.TopPartHeight + _parametres.ProtrusionHeight * 1.5 + _parametres.CrownHeight / 2)).GetAsVector()));
                // Добавление короны к верхней части
                crown.BooleanOperation(BooleanOperationType.BoolSubtract, hole);
                topPart.BooleanOperation(BooleanOperationType.BoolUnite, crown);
            }

            return topPart;
        }
    }
}
