﻿using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerModel.Objects
{
    /// <summary>
    /// Класс, который строит среднюю часть башни
    /// </summary>
    class MiddlePart : IPartDraw
    {
        /// <summary>
        /// Класс параметров
        /// </summary>
        private Parametres _parametres;

        /// <summary>
        /// Возвращаемая сущность
        /// </summary>
        public Entity Entity
        {
            get
            {
                return BuildEntity();
            }
        }


        /// <summary>
        /// Ширина
        /// </summary>
        private int _width;

        /// <summary>
        /// Ширина нижней части
        /// </summary>
        private int _bottomWidth;

        
        
        // Количество сторон пирамиды
        const int SIDES = 4;
        

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="parametres"> Параметры </param>
        /// <param name="bottomPartWidth"> Ширина нижней части </param>
        public MiddlePart(Parametres parametres, int bottomPartWidth)
        {
            _parametres = parametres;
            _bottomWidth = bottomPartWidth;
            _width = bottomPartWidth / 2;
        }

        /// <summary>
        /// Метод построения средней части башни
        /// </summary>
        /// <returns> Сущность средней части </returns>
        private Entity BuildEntity()
        {
            // 70% от высоты центральной части
            int percentageFromCenter = _parametres.CenterPartHeight * 7 / 10;

            Solid3d middlePart = new Solid3d();
            middlePart.CreatePyramid(_parametres.CenterPartHeight, SIDES, _width, _width/2);
            middlePart.Color = Color.FromNames(Properties.Resources.Gray, Properties.Resources.Metallic);
            middlePart.SetDatabaseDefaults();

            //Оси вращение
            CoordinateSystem3d coordinateSystem = Autodesk.AutoCAD.ApplicationServices.Application.
                         DocumentManager.MdiActiveDocument.Editor.CurrentUserCoordinateSystem.CoordinateSystem3d;
            Vector3d xAxis = coordinateSystem.Xaxis;
            Vector3d yAxis = coordinateSystem.Yaxis;
            Vector3d zAxis = coordinateSystem.Zaxis;

            //Смещение центральной части
            middlePart.TransformBy(Matrix3d.Displacement((new Point3d(0, 0,
                             _parametres.BottomPartLength / 2  
                             + _parametres.ProtrusionHeight / 2 + _parametres.CenterPartHeight / 2)).GetAsVector()));

            Solid3d holeX = new Solid3d();
            Solid3d holeY = new Solid3d();
            Solid3d protrusion = new Solid3d();

            holeX.CreateBox(_bottomWidth, _width - 2 * _parametres.BalkThickness,
                           percentageFromCenter);
            holeY.CreateBox(_width - 2 * _parametres.BalkThickness, _bottomWidth,
                            percentageFromCenter);
            
            protrusion.CreateBox(_width + _parametres.ProtrusionWidth, 
                                 _width + _parametres.ProtrusionWidth, _parametres.ProtrusionHeight);
            protrusion.Color = Color.FromNames(Properties.Resources.Gray, Properties.Resources.Metallic);
          

            //Смещение
            holeX.TransformBy(Matrix3d.Displacement((new Point3d(0, 0,
                             percentageFromCenter / 2 + _parametres.BottomPartLength / 2 - _parametres.ProtrusionHeight)).GetAsVector()));
            holeY.TransformBy(Matrix3d.Displacement((new Point3d(0, 0,
                           percentageFromCenter / 2 + _parametres.BottomPartLength / 2 - _parametres.ProtrusionHeight)).GetAsVector()));
            protrusion.TransformBy(Matrix3d.Displacement((new Point3d(0, 0,
                     _parametres.CenterPartHeight + _parametres.BottomPartLength / 2 + _parametres.ProtrusionHeight)).GetAsVector()));

            //Вырезание
            middlePart.BooleanOperation(BooleanOperationType.BoolSubtract, holeX);
            middlePart.BooleanOperation(BooleanOperationType.BoolSubtract, holeY);
            middlePart.BooleanOperation(BooleanOperationType.BoolUnite, protrusion);

            return middlePart;
        }
    }
}
