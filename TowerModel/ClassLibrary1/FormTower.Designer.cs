﻿namespace TowerModel
{
    partial class FormTower
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Build = new System.Windows.Forms.Button();
            this.labelArchRadius = new System.Windows.Forms.Label();
            this.tbArchRadius = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbBottomPartLength = new System.Windows.Forms.TextBox();
            this.tbBalkThickness = new System.Windows.Forms.TextBox();
            this.tbMiddlePartHeight = new System.Windows.Forms.TextBox();
            this.tbCrownHeight = new System.Windows.Forms.TextBox();
            this.tbCrownThickness = new System.Windows.Forms.TextBox();
            this.tbProtrusionHeight = new System.Windows.Forms.TextBox();
            this.tbProtrusionWidth = new System.Windows.Forms.TextBox();
            this.tbTopPartHeight = new System.Windows.Forms.TextBox();
            this.cbIsCrown = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Build
            // 
            this.btn_Build.Location = new System.Drawing.Point(378, 164);
            this.btn_Build.Name = "btn_Build";
            this.btn_Build.Size = new System.Drawing.Size(107, 25);
            this.btn_Build.TabIndex = 0;
            this.btn_Build.Text = "Построить";
            this.btn_Build.UseVisualStyleBackColor = true;
            this.btn_Build.Click += new System.EventHandler(this.btn_Build_Click);
            // 
            // labelArchRadius
            // 
            this.labelArchRadius.AutoSize = true;
            this.labelArchRadius.Location = new System.Drawing.Point(6, 19);
            this.labelArchRadius.Name = "labelArchRadius";
            this.labelArchRadius.Size = new System.Drawing.Size(70, 13);
            this.labelArchRadius.TabIndex = 1;
            this.labelArchRadius.Text = "Радиус арки";
            // 
            // tbArchRadius
            // 
            this.tbArchRadius.Location = new System.Drawing.Point(133, 16);
            this.tbArchRadius.Name = "tbArchRadius";
            this.tbArchRadius.Size = new System.Drawing.Size(51, 20);
            this.tbArchRadius.TabIndex = 2;
            this.tbArchRadius.Text = "500";
            this.tbArchRadius.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbArchRadius_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Высота основания";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(171, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Толщина балки в средней части";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Высота средней части";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Высота короны";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Толщина короны";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 69);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Высота выступа";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 92);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Ширина выступа";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(120, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Высота верхней части";
            // 
            // tbBottomPartLength
            // 
            this.tbBottomPartLength.Location = new System.Drawing.Point(133, 42);
            this.tbBottomPartLength.Name = "tbBottomPartLength";
            this.tbBottomPartLength.Size = new System.Drawing.Size(51, 20);
            this.tbBottomPartLength.TabIndex = 12;
            this.tbBottomPartLength.Text = "1000";
            this.tbBottomPartLength.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbArchRadius_KeyPress);
            // 
            // tbBalkThickness
            // 
            this.tbBalkThickness.Location = new System.Drawing.Point(188, 16);
            this.tbBalkThickness.Name = "tbBalkThickness";
            this.tbBalkThickness.Size = new System.Drawing.Size(51, 20);
            this.tbBalkThickness.TabIndex = 13;
            this.tbBalkThickness.Text = "100";
            this.tbBalkThickness.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbArchRadius_KeyPress);
            // 
            // tbMiddlePartHeight
            // 
            this.tbMiddlePartHeight.Location = new System.Drawing.Point(188, 42);
            this.tbMiddlePartHeight.Name = "tbMiddlePartHeight";
            this.tbMiddlePartHeight.Size = new System.Drawing.Size(51, 20);
            this.tbMiddlePartHeight.TabIndex = 14;
            this.tbMiddlePartHeight.Text = "1000";
            this.tbMiddlePartHeight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbArchRadius_KeyPress);
            // 
            // tbCrownHeight
            // 
            this.tbCrownHeight.Location = new System.Drawing.Point(133, 41);
            this.tbCrownHeight.Name = "tbCrownHeight";
            this.tbCrownHeight.Size = new System.Drawing.Size(51, 20);
            this.tbCrownHeight.TabIndex = 15;
            this.tbCrownHeight.Text = "200";
            this.tbCrownHeight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbArchRadius_KeyPress);
            // 
            // tbCrownThickness
            // 
            this.tbCrownThickness.Location = new System.Drawing.Point(133, 66);
            this.tbCrownThickness.Name = "tbCrownThickness";
            this.tbCrownThickness.Size = new System.Drawing.Size(51, 20);
            this.tbCrownThickness.TabIndex = 16;
            this.tbCrownThickness.Text = "100";
            this.tbCrownThickness.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbArchRadius_KeyPress);
            // 
            // tbProtrusionHeight
            // 
            this.tbProtrusionHeight.Location = new System.Drawing.Point(188, 66);
            this.tbProtrusionHeight.Name = "tbProtrusionHeight";
            this.tbProtrusionHeight.Size = new System.Drawing.Size(51, 20);
            this.tbProtrusionHeight.TabIndex = 17;
            this.tbProtrusionHeight.Text = "100";
            this.tbProtrusionHeight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbArchRadius_KeyPress);
            // 
            // tbProtrusionWidth
            // 
            this.tbProtrusionWidth.Location = new System.Drawing.Point(188, 89);
            this.tbProtrusionWidth.Name = "tbProtrusionWidth";
            this.tbProtrusionWidth.Size = new System.Drawing.Size(51, 20);
            this.tbProtrusionWidth.TabIndex = 18;
            this.tbProtrusionWidth.Text = "100";
            this.tbProtrusionWidth.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbArchRadius_KeyPress);
            // 
            // tbTopPartHeight
            // 
            this.tbTopPartHeight.Location = new System.Drawing.Point(133, 18);
            this.tbTopPartHeight.Name = "tbTopPartHeight";
            this.tbTopPartHeight.Size = new System.Drawing.Size(51, 20);
            this.tbTopPartHeight.TabIndex = 19;
            this.tbTopPartHeight.Text = "1000";
            this.tbTopPartHeight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbArchRadius_KeyPress);
            // 
            // cbIsCrown
            // 
            this.cbIsCrown.AutoSize = true;
            this.cbIsCrown.Checked = true;
            this.cbIsCrown.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbIsCrown.Location = new System.Drawing.Point(6, 28);
            this.cbIsCrown.Name = "cbIsCrown";
            this.cbIsCrown.Size = new System.Drawing.Size(110, 17);
            this.cbIsCrown.TabIndex = 20;
            this.cbIsCrown.Text = "Наличие короны";
            this.cbIsCrown.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.tbArchRadius);
            this.groupBox1.Controls.Add(this.labelArchRadius);
            this.groupBox1.Controls.Add(this.tbBottomPartLength);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(6, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(209, 79);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Нижняя часть";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.tbBalkThickness);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.tbProtrusionWidth);
            this.groupBox2.Controls.Add(this.tbMiddlePartHeight);
            this.groupBox2.Controls.Add(this.tbProtrusionHeight);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Location = new System.Drawing.Point(219, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(266, 119);
            this.groupBox2.TabIndex = 22;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Средняя часть";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.tbTopPartHeight);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.tbCrownHeight);
            this.groupBox3.Controls.Add(this.tbCrownThickness);
            this.groupBox3.Location = new System.Drawing.Point(6, 94);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(209, 95);
            this.groupBox3.TabIndex = 23;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Верхняя часть";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cbIsCrown);
            this.groupBox4.Location = new System.Drawing.Point(219, 134);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(136, 55);
            this.groupBox4.TabIndex = 24;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Наличие элементов";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(185, 19);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "см";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(186, 45);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(21, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "см";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(185, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 13);
            this.label11.TabIndex = 13;
            this.label11.Text = "см";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(185, 44);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(21, 13);
            this.label12.TabIndex = 13;
            this.label12.Text = "см";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(185, 69);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(21, 13);
            this.label13.TabIndex = 13;
            this.label13.Text = "см";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(240, 19);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(21, 13);
            this.label14.TabIndex = 13;
            this.label14.Text = "см";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(240, 45);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(21, 13);
            this.label15.TabIndex = 13;
            this.label15.Text = "см";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(240, 69);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(21, 13);
            this.label16.TabIndex = 13;
            this.label16.Text = "см";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(240, 92);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(21, 13);
            this.label17.TabIndex = 13;
            this.label17.Text = "см";
            // 
            // FormTower
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(489, 193);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_Build);
            this.Name = "FormTower";
            this.Text = "Ввод параметров";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_Build;
        private System.Windows.Forms.Label labelArchRadius;
        private System.Windows.Forms.TextBox tbArchRadius;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbBottomPartLength;
        private System.Windows.Forms.TextBox tbBalkThickness;
        private System.Windows.Forms.TextBox tbMiddlePartHeight;
        private System.Windows.Forms.TextBox tbCrownHeight;
        private System.Windows.Forms.TextBox tbCrownThickness;
        private System.Windows.Forms.TextBox tbProtrusionHeight;
        private System.Windows.Forms.TextBox tbProtrusionWidth;
        private System.Windows.Forms.TextBox tbTopPartHeight;
        private System.Windows.Forms.CheckBox cbIsCrown;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
    }
}