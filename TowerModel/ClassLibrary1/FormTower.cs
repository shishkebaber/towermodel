﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TowerModel.Objects;

namespace TowerModel
{
    /// <summary>
    /// Класс формы
    /// </summary>
    public partial class FormTower : Form
    {
        #region Properties

        /// <summary>
        /// Класс который хранит параметры башни
        /// </summary>
        private Parametres _parametres;

        /// <summary>
        /// Класс в котором происходит построение башни
        /// </summary>
        private Tower _tower;

        /// <summary>
        /// Конструктор
        /// </summary>
        public FormTower()
        {
            InitializeComponent();
        }

        #endregion

        /// <summary>
        /// Метод обрабатывающий нажатие на кнопку "Построить"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Build_Click(object sender, EventArgs e)
        {
            try
            {
                _parametres = new Parametres(
               Convert.ToInt32(tbArchRadius.Text),
               Convert.ToInt32(tbBalkThickness.Text),
               Convert.ToInt32(tbBottomPartLength.Text),
               Convert.ToInt32(tbMiddlePartHeight.Text),
               Convert.ToInt32(tbCrownHeight.Text),
               Convert.ToInt32(tbCrownThickness.Text),
               Convert.ToInt32(tbProtrusionHeight.Text),
               Convert.ToInt32(tbProtrusionWidth.Text),
               Convert.ToInt32(tbTopPartHeight.Text),
               cbIsCrown.Checked);
                _parametres.Validate();
                _tower = new Tower(_parametres);
                _tower.BuildTower();
            }
            catch (FormatException)
            {
                MessageBox.Show(this, "Все поля должны быть заполнены", "Обнаружены ошибки", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (ApplicationException exception)
            {
                MessageBox.Show(this, exception.Message , "Обнаружены ошибки", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (Exception exception)
            {
                MessageBox.Show(this, exception.Message + " \n" + exception.StackTrace, "Обнаружены ошибки", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            

           
           
        }

        /// <summary>
        /// Метод разрешает ввод только цифр и Backspace
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbArchRadius_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != 8)
            {
                e.Handled = true;
            }
        }
    }
}
